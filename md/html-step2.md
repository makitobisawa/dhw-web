# HTMLマークアップ
## HTML5の雛形
HTML5の雛形の例です。（詳細は以降説明）

    <!DOCTYPE html>
    <html lang="ja">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>タイトル</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="css/style.css">
    <style>
    /*remの基準文字サイズ*/
    html {
      font-size: 16px;
    }
    </style>
    </head>
    <body>
    <h1>Hello World!!</h1>
    <script src="js/script.js"></script>
    <script>
    //onloadは一度だけ記述
    window.onload = function() {
      //処理を書いていく
      console.log('onload 1');
    }
    </script>
    </body>
    </html>
    
## HTML5のDOCTYPE宣言
文章がHTML5であることを宣言する。

    <!DOCTYPE html>
    
## htmlタグ
ドキュメントルートと呼ばれる要素。lang属性に言語コードを指定します。

    <html lang="ja">
    </html>
    
## headとbodyタグ
htmlタグには、headとbodyタグを含める。  
headの中には、各種metaタグやtitleタグ、cssの定義などを行う。  
bodyタグの中には、ページコンテンツとJavacriptの記述を行う。

    <head>
    </head>
    <body>
    </body>

## metaタグ：文字コード
文字コードは現在、UTF-8が一般的。

    <meta charset="UTF-8">
    
## metaタグ：viewport
表示領域をどのように認識されるかの定義。
width=device-widthの指定をすると、デバイスの幅（ポイント）に応じて表示する。

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

## IEレンダリングモード
IEが互換性表示設定されているとうまく表示されてないことを回避するため、最新のedgeモードでレンダリングするよう指定。

## titleタグ
検索結果として使用される。日本語で30文字以内を推奨。
SEO対策として重要なので、クリックしたくなるページタイトルをつけるべき。

## metaタグ：description
検索結果として使用される。100〜120文字程度を推奨。
SEO対策として重要なので、クリックしたくなるページタイトルをつけるべき。他のページと被らないようにする。

## スタイルシートの読み込み
headタグ内で行う。外部ファイル化する場合は、ファイル数が多くなると読み込み速度が遅くなることに注意。

## Javascriptの読み込み
bodyの閉じタグの直前に記述。headタグ内に記述すると、ブラウザのレンダリング速度に影響するので注意。
