# レスポンシブ基本設定

## レスポンシブデザインとは
様々な幅を持つデバイスに応じたデザインを定義する方法。  
Photoshop/XDなど、デザインの際は、アートボードを複数（スマホ/タブレット/PC）作成してデザインする。

HTMLにはレスポンシブのmetaタグ（viewport）を設置する。  

    <meta name="viewport" content="width=device-width,initial-scale=1">

HTMLまたはCSSでメディアクエリを記述し、幅に応じたCSSを定義していく。

### htmlの場合のメディアクエリ記述例
CSSを幅に応じて別ファイルで作成する場合の例。

    <link rel="stylesheet" href="sp.css" type="text/css" media="screen and (min-width:320px)">
    <link rel="stylesheet" href="md.css" type="text/css" media="screen and (min-width:768px)">
    <link rel="stylesheet" href="pc.css" type="text/css" media="screen and (min-width:1025px)">

### CSSの場合のメディアクエリの記述例
PCをベースで作成したあとにタブレットやスマホを定義する例。

    /*共通設定、PC版は最初に書く*/

    @media screen and (max-width: 1024px) {
      /*タブレット縦まで*/
    }
    @media screen and (max-width: 480px) {
      /*スマホ縦まで*/
    }


スマホをベースで作成したあとにタブレットやPCを定義する例。

    /*共通設定、スマホ版は最初に書く*/
    　  
    @media screen and (min-width: 768px) {
      /*タブレット縦まで*/
    }
    @media screen and (min-width: 1025px) {
      /*PC版*/
    }

## 主なデバイスの幅一覧

[2019年2月修正追記】レスポンシブ CSSメディアクエリ（@media）ブレイクポイントまとめ](https://www.seojuku.com/blog/responsive-mediaquery.html) 


