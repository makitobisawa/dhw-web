# HTML 概論

## Webサイトの構成

- ドメインを契約（レンタルサーバー会社の契約ドメイン、または独自ドメイン）
- Web公開用サーバー（例：レンタルサーバー）上にFTPを使ってファイル一式を置く

## Webサイトのファイル一式の例

- HTMLファイル（＝ページに対応） .html
- CSSファイル（HTMLからリンクしている、スタイルを定義。複数可） .css
- Javascriptファイル（HTMLからリンクしている、ブラウザ上で動作する機能を実装） .js
- 画像ファイル　.png .jpg .gif .svg

## Webページを作るためのソフトウェア

- 文字コードUTF-8に対応したエディタなら何でもOK。
- Dreamweaver(Adobe) [Dreamweaverチュートリアル](https://helpx.adobe.com/jp/dreamweaver/tutorials.html)
- Brackets [http://brackets.io/](http://brackets.io/)
- ATOM(Github)　[https://atom.io/](https://atom.io/)
- Sublime Text [https://www.sublimetext.com/](https://www.sublimetext.com/)

## 使用するプロトコル

- HTTP HTMLなどのコンテンツの送受信　http://aaa.com デフォルトポート番号：80
- HTTPS HTTPによる通信の暗号化 https://aaa.com デフォルトポート番号：443
- FTP ファイルの転送 デフォルトポート番号：20,21　→FTPソフト[Filezilla](https://ja.osdn.net/projects/filezilla/)
- SSH 暗号化された通信（セキュアシェル）デフォルトポート番号：22


## 主に使用されているHTMLの種類
世の中に公開されているWebサイトは以下のいずれかを使用したものが多い。  
近年ではHTML5を使用。

- HTML4.01（1999年12月24日最終更新）SGML系　→テーブルコーディングの時代
- XHTML1.0Transitional（2002年8月1日改訂）XML系　→CSS2の時代
- HTML5（2014年10月28日最終勧告）SGML系　→CSS3の時代

## CSS2とCSS3の違い
特にバージョンの記述をするわけではなく、規格により新しい機能が定義されている。  
現在どのブラウザでどの機能が使えるかは、Can I useサイトで確認すると良い。  
[Can I use](https://caniuse.com/)

## 使用するブラウザ
制作においては、以下のブラウザで動作確認することが多い。  
各ブラウザでは、開発インスペクタでソースなどを解析することができる。  
使い方は以下。

- Chrome ページ内で右クリックし「検証」
- Safari 環境設定>詳細>メニューバーに"開発"メニューを表示　にチェック。開発>Webインスペクタを表示。
- Microfost IE11(インターネット・エクスプローラー)、Edge ツール>F12開発者ツール
- Firefox 右クリックし「要素を調査」

インスペクタでは、ブラウザが解釈した現在のページのソースを表示できる。（Javascriptなどが実行された場合は事後の状態）  
Javascript開発では、コンソール表示やステップ実行なども利用できる。

古いバージョンや各種ブラウザ、スマホでの表示確認（エミュレーター機能）などの機能もある。（ブラウザにより異なる）

純粋なHTMLファイルのソースを見たい場合は、右クリックして「ページのソースを表示」する。

## Webページをつくる際のワークフロー
1. PhotoshopやWord文書など、ページのテキストデータをHTMLへ移植し、タグでマークアップする。
1. Photoshopなどのファイルより、画像で書き出したものをimgタグでマークアップ。
1. ヘッダー、メインコンテンツ、サイドバー、フッター、などを区切り、セクション系タグでマークアップ。
1. CSSやJSの外部ファイルを作成し、HTMLへリンク。
1. Photoshopなどのファイルより、画像で書き出したり、文字サイズや、色、グラデーション、透明度などの情報を取得し、CSSを記述していく。
1. スタイルの都合などにより、HTMLにタグやクラス、IDを追加しながら、CSSを記述していく。

## レスポンシブデザインとは
様々な幅を持つデバイスに応じたデザインをスタイルシートへ記述する方法。  
Photoshopでは、アートボードを複数（モバイル/iPad/PC）作成してデザインする。

HTMLにはレスポンシブのmetaタグ（viewport）を設置する。  

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes,maximum-scale=2" />

HTMLまたはCSSでメディアクエリを記述し、幅に応じたCSSを定義していく。

### htmlの場合のメディアクエリ記述例
CSSを幅に応じて別ファイルで作成する場合の例。

    <link rel="stylesheet" href="sp.css" type="text/css" media="screen and (min-width:320px)">
    <link rel="stylesheet" href="md.css" type="text/css" media="screen and (min-width:768px)">
    <link rel="stylesheet" href="pc.css" type="text/css" media="screen and (min-width:1000px)">

### CSSの場合のメディアクエリの記述例
モバイルをベースで作成したあとにPC等を定義する例。

    /*スマホで適用する内容*/
     
    @media screen and (min-width:768px){
      /*768px以上(iPad以上)で適用する内容*/
    }
    
    @media screen and (min-width:1000px){
      /*1000px以上（PC）で適用する内容*/
    }

PCをベースで作成したあとにモバイル等を定義する例。

    /*PCで適用する内容*/
    　  
    @media screen and (max-width:999px){
      /*999px以下(iPad等)で適用する内容*/
    }
    
    @media screen and (max-width:767px){
      /*767px以下（スマホ）で適用する内容*/
    }

## 主なデバイスの幅一覧

[2017年版｜パソコン画面解像度シェア&HP制作に最適なサイズとは？](http://mw-s.jp/2017-pc-monitor-share/)  
[Webサイトデザインの横幅サイズ！もう何pxか迷わない！ 2017年1月版](https://fastcoding.jp/blog/all/webdesign/designswidth/)
