# web fontの使い方

webフォントは、デバイスに限らず、同じフォントでページを見せたい場合に使用する。
サイトデータとしてフォントファイルがブラウザに読み込まれて使用される。

## Abobe Fonts
Adobe CCには、Abobe Fonts(旧：Typekit）というフォントサービスがバンドルされている。
使い方は「キット」を作成してフォントを選択後公開し、JSを貼り付け、CSSでfont-familyを指定する。

- [Abobe Fonts](https://fonts.adobe.com/fonts)
- [使い方：Typekit からのフォントの追加](https://helpx.adobe.com/jp/creative-cloud/help/add-fonts.html)
- [使い方：Web サイトへのフォントの追加](https://helpx.adobe.com/jp/fonts/using/add-fonts-website.html)

## Google Fonts

Googleではフリーで使用できるフォントを多数提供している。

## 欧文フォント： google fontの場合
- [Google Fonts](https://fonts.google.com/)

## 和文フォント： google fontの場合
- [Japanese](https://fonts.google.com/?subset=japanese)
- [Google Fonts + Japanese Early Access](https://googlefonts.github.io/japanese/)


### 好きなフォントを選んで、＋マークでピックアップ。
![http://image.frog-eight.com/dhw-web/googlefont.png](http://image.frog-eight.com/dhw-web/googlefont.png)

## 使い方

### HTMLにフォントCSSをリンク。

    <link href="https://fonts.googleapis.com/css?family=Overlock+SC" rel="stylesheet">

### 作成中ページのCSS内で、特定の要素にfont-familyの定義を追加。

    body{
        font-family: 'Overlock SC', cursive;
    }

## CSSの設定による読み込みは、先頭から該当したものが１文字単位で適用されます。

font-famiryには、フォント名を複数指定することが可能です。フォントが存在しない場合や、読み込みエラーなどを配慮して、下記のシステムデフォルトフォントを定義しておくとよいでしょう。

### デフォルトプロパティの名称例

- sans-serif	ゴシック系のフォント
- serif	明朝系のフォント
- cursive	筆記体・草書体のフォント
- fantasy	装飾的なフォント
- monospace	等幅フォント
- inherit	祖先要素の指定を継承します


