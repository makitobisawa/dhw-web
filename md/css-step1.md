# CSS 初期設定
## ブラウザのデフォルトスタイル
各ブラウザには、デフォルトのCSSが適用されている。
検証ツールから要素を確認すると、「user agent stylesheet」と表示され適用されている部分。

## Reset CSS
リセットCSSとは、ブラウザの持つCSSをリセット（基本スタイルを打ち消す）するためのもの。  
CSSをリセットしてから、オリジナルのスタイルを定義していく場合に、最初に読み込ませるCSSファイル。
自分で作成してもよいし、YUIなどのライブラリを使用してもよい。  
CDNにてファイルが提供されている場合は、それを利用したほうが読み込み速度が早くなる。（再利用性が高い為）

meyer-reset

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" />

YUI Reset CSS

    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssreset/cssreset-min.css">
    
## Normalize css
ノーマライズCSSとは、各ブラウザのデフォルトスタイルを平準化して補正したもの。
ブラウザのデフォルトスタイルはベースとして使っていきたい場合に、最初に読み込ませるCSSファイル。

Normalize.css

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

## CSS フレームワーク
CSSフレームワークとは、よく使うスタイルのパターンをライブラリとして配布しているもの。

Bootstrap

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

foundation

    <link href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css" rel="stylesheet" type="text/css">

## CSS雛形

    @charset "UTF-8";
    
    /* 1.Common */
    
    html{
      font-size: 16px;
      font-family: serif;
    }


