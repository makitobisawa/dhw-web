//inview を使うためのJS。ivクラスのついた要素がスクロールして見えるタイミングでiv-のアニメーションクラスを付与。
$(function(){
  var t;
  var ani;
  $('.iv').on('inview', function(event, isInView) {
   if (isInView) {
    t = $(this).attr("data-time") ? $(this).attr("data-time") : 0;
    $(this).delay(t).queue(function(){
     ani = $(this).attr("class").match(/iv\-([A-Za-z]+)/);
     $(this).addClass('animated '+ani[1]);
     $(this).css('opacity',1);
    });
   } else {
    ani = $(this).attr("class").match(/iv\-([A-Za-z]+)/);
    $(this).removeClass('animated '+ani[1]);
    $(this).css('opacity',0);
    $(this).dequeue();
   }
  });
});